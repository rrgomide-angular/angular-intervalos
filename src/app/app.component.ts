import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  divisors = [2, 3, 4, 5, 6, 7, 8, 9];
  currentValue = 1;

  getDivisiveisPor(num) {
    const numbers = [];

    for (let i = 1; i <= this.currentValue; i++) {
      if (i % num === 0) {
        numbers.push(i);
      }
    }

    return numbers;
  }
}
